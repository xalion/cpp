#include <iostream>
#include <cstdlib>

using namespace std;

int main(int argc, char* argv[]) {
    if (argc == 2) {

        if (atoi(argv[1]) < 0) {
            cout << "\033[1m\033[38;5;27mEnter nonnegative number\033[0m\n\n";
        } else {
            srand( time(NULL) );

            char generated_symbol = 0;
            short length_string = atoi(argv[1]);
            string random_string;


            for (short i = 0; i < length_string; i++) {
                generated_symbol = 33 + rand() % 93;
                random_string += generated_symbol;
            }

            cout << "Your password: \033[1m\033[38;5;48m" << random_string << "\033[0m\n\n";
        }

    } else {
        cout << "\033[1m\033[38;5;27mEnter only the length of the password\nExample: ./name_programm 8\033[0m\n\n";
    }

    return 0;
}
